# Ryan Petroff  
# Fullstack Creative Technologist, Developer and Designer
#### Please contact me at petroff.ryan@gmail.com to discuss employment opportunities in Toronto or remote.

Keywords: Fullstack, Javascript, NodeJS, NPM, PYTHON, Arduino, UX, UI, HTML, CSS, JS, 3D, Design, Linux, Git, Agile, Teaching, Leadership, Free Software, Open Source, Science, Security, Mandarin

Experience
==========
Chief Creative Technologist 
---------------------------
##### at Maker Academy
- Founded and operated education company that delivered game development and 3D printing classes to students in public schools. 
- Designed curricula, hired and trained educators, facilitated classes, managed and maintained hardware including laptops and 3D printers. 
- Printed and delivered hundreds of [3D objects](http://www.thingiverse.com/groups/makeracademy/things) to delighted student designers.

Program Designer and Facilitator
--------------------------------
##### at Digital Media Academy, Toronto Tool Library, Focus Learning, and similar organizations 
- Designed and delivered technical education classes and camps, including game design in PYTHON, Javascript, Java, Scratch, and robotics using Arduino, LEGO EV3, Multiplo. 
- International teaching and tutoring in Math, English, Science, Philosophy, and working with special needs students. 
- To be convinced, please see attached reference letters. [1](https://gitlab.com/petroff.ryan/ryan-petroff-resume/blob/master/referenceletter001redactedx.pdf) [2](https://gitlab.com/petroff.ryan/ryan-petroff-resume/blob/master/referenceletter002redactedx.pdf) [3](https://gitlab.com/petroff.ryan/ryan-petroff-resume/blob/master/referenceletter003.pdf)

Digital Mercenary 
------------------
#### Freelance Consultant and Web Developer
- Design and deliver websites and web assets, from using Photoshop and the Adobe suite, to setting up [Wordpress (archive link)](https://web.archive.org/web/20160329135629/http://www.coutureclosets.ca/) or e-commerce sites with [Shopify (archive link)](https://web.archive.org/web/20150211190655/http://cakescove.com/). 
- Develop digital strategies for clients. Prevent waste with Lean Startup design thinking and Customer Development. 
- Fix electronic hardware, including computers and DJ equipment. Fix anything! Thus the nickname "The Troubleshooter".
- Train office staff in software and hardware use, including the Microsoft Office Suite, the Google Suite, and printers, scanners, copiers, and other office equipment.

Education
=========
Completed 4 years in Philosophy and East Asian Studies at University of Toronto
-------------------------------------------------------------------------------
  - Independent study of 3D scanning and printing, presented as guest lecture at OCAD
  - Audited residence building security, leading to replacement of digital door locks
  - Independent study of Artificial Intelligence, Gödel, Minds and Machines
  - Elected and re-elected to small presidencies in the student government

Completed 1 year at Capital Normal University in Beijing
--------------------------------------------------------
  - Won a full scholarship as a guest of the Chinese Government
  - Achieved fluency in Mandarin Chinese, taught English and Philosophy


Extra Credit
============
#### Retail Manager 
- Set up all fixtures and displays of new drone store, front line sales, hired and trained staff, advised international management team on complying with Canadian laws.

#### Volunteer Coordinator and Facilitator 
- Facilitator and Volunteer Coordinator at Atomic Lollipop. 
- Judge for the Canadian Pomological Society.
- Mentor to young entrepreneurs through the Zero To Startup program. 
- Facilitator at Maker Festival, Maker Faire, and other conventions. 

#### Lighthearted Developer 
- Contributes back to open source projects, like [Archey](https://github.com/mixu/archey.js/pull/6).
- Participates in charitable hackathons such as [TPL's Open Data Hackathon](http://torontopubliclibrary.typepad.com/digital_design_studio/2016/09/recap-tpls-second-annual-hackathon-toronto-poverty-reduction-strategy.html) and [Gift The Code](http://giftthecode.ca/blog/Celebrating/). 
- Published a toy package on NPM. Play with a frontend for it here: [Imagineer!](http://petroff.ryan.gitlab.io/imagineerFrontend/)
- Actively seeking new opportunities! Contact petroff.ryan@gmail.com
