# Hire Ryan Petroff Today!

[Please have a look at my resume here](https://gitlab.com/petroff.ryan/ryan-petroff-resume/blob/master/resumeRyanPetroff.md)

Welcome to my resume repo. Here you will find my resume itself and some reference letters.

I can work miracles for your organization if you let me. Please contact me at petroff.ryan@gmail.com to discuss what I can do for you.

If you ever have any questions or comments, or requests for documents you think should be hosted here, please don't hesitate to contact me!
